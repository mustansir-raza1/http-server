const http = require("http");
const fs = require("fs");
const { v4: uuidv4 } = require("uuid")
const statusCode = require("http").STATUS_CODES;

// create server
const server = http.createServer((request, response) => {

    //reading html content
  if (request.method === "GET" && request.url === "/html") {
    const htmlContent = fs.readFileSync("../data/quote.html", "utf-8");
    response.end(htmlContent);
  }

  // reading json file 
  else if (request.method === "GET" && request.url == "/json") {
    const jsonData = fs.readFileSync("../data/data.JSON", "utf-8");
    response.end(jsonData);
  }

  // reading uuid
  else if (request.method === "GET" && request.url === "/uuid") {
    const uuidObj = {
      uuid: uuidv4(),
    };
    response.end(JSON.stringify(uuidObj));
  }

  //statuscode 
  else if (request.method === "GET" && request.url.match("/status/?")) {
    const code = request.url.split("/")[2];
    if (code !== NaN) {
      response.end(statusCode[code]);
    } else {
      response.end("not found it is not number");
    }
  }

  // delay time
  else if (request.method === "GET" && request.url.match("/delay/?")) {
    const delayTime = request.url.split("/")[2];
    if (delayTime !== NaN) {
      setTimeout(() => {
        response.end(statusCode[200]);
      }, delayTime * 1000);
    }
    else {
      response.end("input is not a number");
    }
  }  
});
server.listen(8000);
console.log("started");
